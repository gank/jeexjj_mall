<#--
/****************************************************
 * Description: t_mall_order_shipping的输入页面，包括添加和修改
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl"> 

<@input url="${base}/mall/order/shipping/save" id=tabId>
   <input type="hidden" name="id" value="${orderShipping.id}"/>
   
   <@formgroup title='订单ID'>
	<input type="text" name="orderId" value="${orderShipping.orderId}" check-type="required">
   </@formgroup>
   <@formgroup title='收货人全名'>
	<input type="text" name="receiverName" value="${orderShipping.receiverName}" >
   </@formgroup>
   <@formgroup title='固定电话'>
	<input type="text" name="receiverPhone" value="${orderShipping.receiverPhone}" >
   </@formgroup>
   <@formgroup title='移动电话'>
	<input type="text" name="receiverMobile" value="${orderShipping.receiverMobile}" >
   </@formgroup>
   <@formgroup title='省份'>
	<input type="text" name="receiverState" value="${orderShipping.receiverState}" >
   </@formgroup>
   <@formgroup title='城市'>
	<input type="text" name="receiverCity" value="${orderShipping.receiverCity}" >
   </@formgroup>
   <@formgroup title='区/县'>
	<input type="text" name="receiverDistrict" value="${orderShipping.receiverDistrict}" >
   </@formgroup>
   <@formgroup title='收货地址，如：xx路xx号'>
	<input type="text" name="receiverAddress" value="${orderShipping.receiverAddress}" >
   </@formgroup>
   <@formgroup title='邮政编码,如：310001'>
	<input type="text" name="receiverZip" value="${orderShipping.receiverZip}" >
   </@formgroup>
   <@formgroup title='created'>
	<@date name="created" dateValue=orderShipping.created  default=true/>
   </@formgroup>
   <@formgroup title='updated'>
	<@date name="updated" dateValue=orderShipping.updated  default=true/>
   </@formgroup>
</@input>