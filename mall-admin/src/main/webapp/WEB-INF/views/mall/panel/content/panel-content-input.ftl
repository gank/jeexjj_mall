<#--
/****************************************************
 * Description: t_mall_panel_content的输入页面，包括添加和修改
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl"> 

<@input url="${base}/mall/panel/content/save" id=tabId>
   <input type="hidden" name="id" value="${panelContent.id}"/>
   
   <@formgroup title='所属板块id'>
	<input type="text" name="panelId" value="${panelContent.panelId}" check-type="required number">
   </@formgroup>
   <@formgroup title='类型 0关联商品 1其他链接'>
	<input type="text" name="type" value="${panelContent.type}" check-type="number">
   </@formgroup>
   <@formgroup title='关联商品id'>
	<input type="text" name="productId" value="${panelContent.productId}" check-type="number">
   </@formgroup>
   <@formgroup title='sort_order'>
	<input type="text" name="sortOrder" value="${panelContent.sortOrder}" check-type="number">
   </@formgroup>
   <@formgroup title='其他链接'>
	<input type="text" name="fullUrl" value="${panelContent.fullUrl}" >
   </@formgroup>
   <@formgroup title='pic_url'>
	<input type="text" name="picUrl" value="${panelContent.picUrl}" >
   </@formgroup>
   <@formgroup title='3d轮播图备用'>
	<input type="text" name="picUrl2" value="${panelContent.picUrl2}" >
   </@formgroup>
   <@formgroup title='3d轮播图备用'>
	<input type="text" name="picUrl3" value="${panelContent.picUrl3}" >
   </@formgroup>
   <@formgroup title='created'>
	<@date name="created" dateValue=panelContent.created  default=true/>
   </@formgroup>
   <@formgroup title='updated'>
	<@date name="updated" dateValue=panelContent.updated  default=true/>
   </@formgroup>
</@input>