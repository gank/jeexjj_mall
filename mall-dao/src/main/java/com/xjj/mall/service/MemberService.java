package com.xjj.mall.service;

import com.xjj.framework.service.XjjService;
import com.xjj.mall.entity.MemberEntity;

/**
 * @author zhanghejie
 */
public interface MemberService extends XjjService<MemberEntity>{

    /**
     * 头像上传
     * @param userId
     * @param token
     * @param imgData
     * @return
     */
    String imageUpload(Long userId,String token,String imgData);
}
